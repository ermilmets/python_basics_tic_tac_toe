# Python_basics_tic_tac_toe

import random  # this is to generate a random number

myboard = ["-", "-", "-", "-", "-", "-", "-", "-", "-"]  # empty board

def start_the_game(difficulty):
    print("Get ready for a game of TicTacToe!!")
    choose_random_player = random.randint(0, 1)  # this determines a random binary number
    if difficulty == "a":
        if choose_random_player == 0:
            print("Player 1 won the coin toss and goes first")
            play_the_game_player1(myboard, difficulty)
        else:
            print("Player 2 won the coin toss and goes first")
            play_the_game_player2(myboard, difficulty)
    elif difficulty == "b":
        if choose_random_player == 0:
            print("You(Player 1) won the coin toss and go first")
            play_the_game_player1(myboard, difficulty)
        else:
            print("Computer won the coin toss and goes first")
            play_the_game_dummy_pc(myboard, difficulty)
    elif difficulty == "c":
        if choose_random_player == 0:
            print("You(Player 1) won the coin toss and go first")
            play_the_game_player1(myboard, difficulty)
        else:
            print("Computer won the coin toss and goes first")
            play_the_game_expert_pc(myboard, difficulty)

def print_board(myboard):
    print(myboard[0:3])
    print(myboard[3:6])
    print(myboard[6:])

def check_if_board_filled(myboard): # this checks if the board is filled or not
    for i in myboard:
        if i == "-":
            return False
    return True

def check_if_board_empty(myboard): # for the first move for expert pc
    for i in myboard:
        if i != "-":
            return False
    return True

def check_possible_win(myboard): # This checks possible win layouts and returns winning index
    if myboard[0] == "O" and myboard[1] == "O" and myboard[2] == "-":
            myboard[2] = "O"
            return myboard[2]
    elif myboard[0] == "O" and myboard[1] == "-" and myboard[2] == "O":
            myboard[1] = "O"
            return myboard[1]
    elif myboard[0] == "-" and myboard[1] == "O" and myboard[2] == "O":
            myboard[0] = "O"
            return myboard[0]  # first row end
    elif myboard[3] == "O" and myboard[4] == "O" and myboard[5] == "-":
            myboard[5] = "O"
            return myboard[5]
    elif myboard[3] == "O" and myboard[4] == "-" and myboard[5] == "O":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[3] == "-" and myboard[4] == "O" and myboard[5] == "O":
            myboard[3] = "O"
            return myboard[3]  # second row end
    elif myboard[6] == "O" and myboard[7] == "O" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[6] == "O" and myboard[7] == "-" and myboard[8] == "O":
            myboard[7] = "O"
            return myboard[7]
    elif myboard[6] == "-" and myboard[7] == "O" and myboard[8] == "O":
            myboard[6] = "O"
            return myboard[6]  # Third row end
    elif myboard[0] == "O" and myboard[3] == "O" and myboard[6] == "-":
            myboard[6] = "O"
            return myboard[6]
    elif myboard[0] == "O" and myboard[3] == "-" and myboard[6] == "O":
            myboard[3] = "O"
            return myboard[3]
    elif myboard[0] == "-" and myboard[3] == "O" and myboard[6] == "O":
            myboard[0] = "O"
            return myboard[0]  # First Column end
    elif myboard[1] == "O" and myboard[4] == "O" and myboard[7] == "-":
            myboard[7] = "O"
            return myboard[7]
    elif myboard[1] == "O" and myboard[4] == "-" and myboard[7] == "O":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[1] == "-" and myboard[4] == "O" and myboard[7] == "O":
            myboard[1] = "O"
            return myboard[1]  # Second Column end
    elif myboard[2] == "O" and myboard[5] == "O" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[2] == "O" and myboard[5] == "-" and myboard[8] == "O":
            myboard[5] = "O"
            return myboard[5]
    elif myboard[2] == "-" and myboard[5] == "O" and myboard[8] == "O":
            myboard[2] = "O"
            return myboard[2]    # Third column end
    elif myboard[0] == "O" and myboard[4] == "O" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[0] == "O" and myboard[4] == "-" and myboard[8] == "O":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[0] == "-" and myboard[4] == "O" and myboard[8] == "O":
            myboard[0] = "O"
            return myboard[0]  # First diagonal
    elif myboard[2] == "O" and myboard[4] == "O" and myboard[6] == "-":
            myboard[6] = "O"
            return myboard[6]
    elif myboard[2] == "O" and myboard[4] == "-" and myboard[6] == "O":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[2] == "-" and myboard[4] == "O" and myboard[6] == "O":
            myboard[2] = "O"
            return myboard[2]  # Second diagonal
    else:
        return False

def check_possible_loss(myboard):   # This checks for possible loss conditions and prevents them
    if myboard[0] == "X" and myboard[1] == "X" and myboard[2] == "-":
            myboard[2] = "O"
            return myboard[2]
    elif myboard[0] == "X" and myboard[1] == "-" and myboard[2] == "X":
            myboard[1] = "O"
            return myboard[1]
    elif myboard[0] == "-" and myboard[1] == "X" and myboard[2] == "X":
            myboard[0] = "O"
            return myboard[0]  # first row end
    elif myboard[3] == "X" and myboard[4] == "X" and myboard[5] == "-":
            myboard[5] = "O"
            return myboard[5]
    elif myboard[3] == "X" and myboard[4] == "-" and myboard[5] == "X":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[3] == "-" and myboard[4] == "X" and myboard[5] == "X":
            myboard[3] = "O"
            return myboard[3]  # second row end
    elif myboard[6] == "X" and myboard[7] == "X" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[6] == "X" and myboard[7] == "-" and myboard[8] == "X":
            myboard[7] = "O"
            return myboard[7]
    elif myboard[6] == "-" and myboard[7] == "X" and myboard[8] == "X":
            myboard[6] = "O"
            return myboard[6]  # Third row end
    elif myboard[0] == "X" and myboard[3] == "X" and myboard[6] == "-":
            myboard[6] = "O"
            return myboard[6]
    elif myboard[0] == "X" and myboard[3] == "-" and myboard[6] == "X":
            myboard[3] = "O"
            return myboard[3]
    elif myboard[0] == "-" and myboard[3] == "X" and myboard[6] == "X":
            myboard[0] = "O"
            return myboard[0]  # First Column end
    elif myboard[1] == "X" and myboard[4] == "X" and myboard[7] == "-":
            myboard[7] = "O"
            return myboard[7]
    elif myboard[1] == "X" and myboard[4] == "-" and myboard[7] == "X":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[1] == "-" and myboard[4] == "X" and myboard[7] == "X":
            myboard[1] = "O"
            return myboard[1]  # Second Column end
    elif myboard[2] == "X" and myboard[5] == "X" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[2] == "X" and myboard[5] == "-" and myboard[8] == "X":
            myboard[5] = "O"
            return myboard[5]
    elif myboard[2] == "-" and myboard[5] == "X" and myboard[8] == "X":
            myboard[2] = "O"
            return myboard[2]   # Third column end
    elif myboard[0] == "X" and myboard[4] == "X" and myboard[8] == "-":
            myboard[8] = "O"
            return myboard[8]
    elif myboard[0] == "X" and myboard[4] == "-" and myboard[8] == "X":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[0] == "-" and myboard[4] == "X" and myboard[8] == "X":
            myboard[0] = "O"
            return myboard[0]  # First diagonal
    elif myboard[2] == "X" and myboard[4] == "X" and myboard[6] == "-":
            myboard[6] = "O"
            return myboard[6]
    elif myboard[2] == "X" and myboard[4] == "-" and myboard[6] == "X":
            myboard[4] = "O"
            return myboard[4]
    elif myboard[2] == "-" and myboard[4] == "X" and myboard[6] == "X":
            myboard[2] = "O"
            return myboard[2]  # Second diagonal
    else:
        return False

def check_win(myboard): # This checks for all of the win conditions
    if (myboard[0] == "X" and myboard[1] == "X" and myboard[2] == "X" or
            myboard[0] == "O" and myboard[1] == "O" and myboard[2] == "O"):
        return True
    elif (myboard[3] == "X" and myboard[4] == "X" and myboard[5] == "X" or
          myboard[3] == "O" and myboard[4] == "O" and myboard[5] == "O"):
        return True
    elif (myboard[6] == "X" and myboard[7] == "X" and myboard[8] == "X" or
          myboard[6] == "O" and myboard[7] == "O" and myboard[8] == "O"):
        return True
    elif (myboard[0] == "X" and myboard[3] == "X" and myboard[6] == "X" or
          myboard[0] == "O" and myboard[3] == "O" and myboard[6] == "O"):
        return True
    elif (myboard[1] == "X" and myboard[4] == "X" and myboard[7] == "X" or
          myboard[1] == "O" and myboard[4] == "O" and myboard[7] == "O"):
        return True
    elif (myboard[2] == "X" and myboard[5] == "X" and myboard[8] == "X" or
          myboard[2] == "O" and myboard[5] == "O" and myboard[8] == "O"):
        return True
    elif (myboard[0] == "X" and myboard[4] == "X" and myboard[8] == "X" or
          myboard[0] == "O" and myboard[4] == "O" and myboard[8] == "O"):
        return True
    elif (myboard[2] == "X" and myboard[4] == "X" and myboard[6] == "X" or
          myboard[2] == "O" and myboard[4] == "O" and myboard[6] == "O"):
        return True
    else:
        return False

def play_the_game_player1(myboard, difficulty):  # This starts the turn for player 1
    print_board(myboard)
    userinput = int()
    userinput1 = int(input("Player 1, please enter a ROW number from 1 to 3 to insert \"X\" into: ") or int(4))
    userinput2 = int(input("Player 1, please enter a COLUMN number from 1 to 3 to insert \"X\" into: ") or int(4))
    if userinput1 > 3 or userinput1 < 1 or userinput2 > 3 or userinput2 < 1:
        print("Error, value outside of the board, please try again.")
        play_the_game_player1(myboard, difficulty)
    if userinput1 == 1:
        userinput = userinput2 - 1
    elif userinput1 == 2:
        userinput = userinput2 + 2
    elif userinput1 == 3:
        userinput = userinput2 + 5 # this gets the exact list index value to use in myboard list
    # this version below uses only the list indexes
    #userinput = int(input("Player 1, enter a number from 0 to 8 to insert X into: "))  #this is range of list
    #if userinput > 8 or userinput < 0:
    #    print(userinput, " is not within the range of 0 to 8, please try again.")
    #    play_the_game_player1(myboard)
    if myboard[userinput] == "X" or myboard[userinput] == "O":
        print("Error, this space is already occupied, please try again.")
        play_the_game_player1(myboard, difficulty)
    else:
        myboard[userinput] = "X"
    if check_win(myboard):
        print_board(myboard)
        print("Player 1 WINS!")
        print("Game Over")
        exit()
    elif check_if_board_filled(myboard):
        print_board(myboard)
        print("No winner, it's a TIE!")
        print("Game Over")
        exit()
    else:
        if difficulty == "a":
            play_the_game_player2(myboard, difficulty)
        elif difficulty == "b":
            play_the_game_dummy_pc(myboard, difficulty)
        elif difficulty == "c":
            play_the_game_expert_pc(myboard, difficulty)
        else:
            exit()

def play_the_game_player2(myboard, difficulty):  # This starts the turn for player 2
    print_board(myboard)
    # this version uses rows and columns
    userinput = int()
    userinput1 = int(input("Player 2, please enter a ROW number from 1 to 3 to insert \"O\" into: ") or int(4))
    userinput2 = int(input("Player 2, please enter a COLUMN number from 1 to 3 to insert \"O\" into: ") or int(4))
    if userinput1 > 3 or userinput1 < 1 or userinput2 > 3 or userinput2 < 1:
        print("Error, value outside of the board, please try again.")
        play_the_game_player2(myboard, difficulty)
    if userinput1 == 1:
        userinput = userinput2 - 1
    elif userinput1 == 2:
        userinput = userinput2 + 2
    elif userinput1 == 3:
        userinput = userinput2 + 5
    # this version below uses only the list indexes
    #userinput = int(input("Player 2, enter a number from 0 to 8 to insert O into: "))
    #if userinput > 8 or userinput < 0:
    #    print(userinput, " is not within the range of 0 to 8, please try again.")
    #    play_the_game_player2(myboard)
    if myboard[userinput] == "X" or myboard[userinput] == "O":
        print("Error, this space is already occupied, please try again.")
        play_the_game_player2(myboard, difficulty)
    else:
        myboard[userinput] = "O"
    if check_win(myboard):
        print_board(myboard)
        print("Player 2 WINS!")
        print("Game Over")
        exit()
    elif check_if_board_filled(myboard):
        print_board(myboard)
        print("No winner, it's a TIE!")
        print("Game Over")
        exit()
    else:
        play_the_game_player1(myboard, difficulty)


def game_difficulty_choice():
    print("Welcome to Tictactoe Complete Edition v1.1!")
    print("(Includes DLC: Expert CPU!)")
    print("Created by: Ermil Mets")
    game_mode = str()
    human = "Human vs Human"
    easy = "Human vs CPU(Easy)"
    hard = "Human vs CPU(Expert)"
    print("Available game difficulties:")
    print(human, "(a)")
    print(easy, "(b)")
    print(hard, "(c)")
    userinput = input("Please choose your game difficulty (a/b/c): ")
    while userinput != "a" and userinput != "b" and userinput != "c":
        print("Invalid choice, please try again.")
        userinput = input("Please choose your game difficulty (a/b/c): ")
    if userinput == "a":
        game_mode = human
    elif userinput == "b":
        game_mode = easy
    elif userinput == "c":
        game_mode = hard
    print("You have chosen: ", userinput.upper(), f"<{game_mode}>")
    userinput1 = str(input("Do you wish to start the game? (y/n): "))
    while userinput1 != "n" and userinput1 != "y":
        print("Please enter y or n!")
        userinput1 = str(input("Do you wish to start the game? (y/n): "))
    if userinput1 == "n":  # or userinput1 == "n":
        print("Quitting game.")
        exit()
    elif userinput1 == "y":
        return userinput  # this returns either a or b or c


def play_the_game_dummy_pc(myboard, difficulty):  # This starts the turn for pc in easy mode
    print_board(myboard)
    print("The Computer(EASY) makes it's move: ")
    # this version below uses only the list indexes
    userinput = random.randint(0,8)
    while myboard[userinput] == "X" or myboard[userinput] == "O":
        userinput = random.randint(0,8)
    myboard[userinput] = "O"
    if check_win(myboard):
        print_board(myboard)
        print("Computer(EASY) WINS!")
        print("Game Over")
        exit()
    elif check_if_board_filled(myboard):
        print_board(myboard)
        print("No winner, it's a TIE!")
        print("Game Over")
        exit()
    else:
        play_the_game_player1(myboard, difficulty)

def play_the_game_expert_pc(myboard, difficulty):  # This starts the turn for pc in expert mode
    print_board(myboard)
    print("The Computer(EXPERT) makes it's move: ")
    if check_possible_win(myboard):  # Checks win for pc
        print_board(myboard)
        print("Computer(EXPERT) WINS!")
        print("Game Over")
        exit()
    elif check_possible_loss(myboard):  # Prevents loss for CPU
        if check_if_board_filled(myboard):
            print_board(myboard)
            print("No winner, it's a TIE!")
            print("Game Over")
            exit()
        else:
            play_the_game_player1(myboard, difficulty)  # Switches to Player 1
    else:
        diagonal_list = [0, 2, 6, 8]
        cross_list = [1, 3, 5, 7]
        extra_list = [myboard[0], myboard[2], myboard[6], myboard[8]]
        if check_if_board_empty(myboard):  # When going first
            myboard[4] = "O"
        elif myboard.count("-") == 8 and myboard[4] == "-":  # When going second
            myboard[4] = "O"
        elif myboard.count("-") == 8 and myboard[4] != "-":
            myboard[random.choice(diagonal_list)] = "O"  # picks a random diagonal when going second in turn 1
        elif myboard.count("-") == 7 and extra_list.count("X") == 1:
            random_choice = random.choice(diagonal_list)
            while myboard[random_choice] != "-":
                random_choice = random.choice(diagonal_list)
            myboard[random_choice] = "O"  # picks a random diagonal when going first in turn 2
        elif myboard.count("-") == 6 and extra_list.count("X") == 2:  # necessary, prevents trickery
            myboard[random.choice(cross_list)] = "O"  # picks a random cross when going second in turn 2
        elif myboard.count("-") == 6 and extra_list.count("X") == 1 and extra_list.count("O") == 1:  # necessary x2
            random_choice1 = random.choice(diagonal_list)
            while myboard[random_choice1] != "-":
                random_choice1 = random.choice(diagonal_list)
            myboard[random_choice1] = "O"  # picks an unoccupied diagonal when going second turn 2
        else:
            random_number = random.randint(0, 8)
            while myboard[random_number] != "-":   # or myboard[random_number] == "O":
                random_number = random.randint(0, 8)
            myboard[random_number] = "O"  # there's always free space left, won't glitch
    if check_win(myboard):
        print_board(myboard)
        print("Computer(EXPERT) WINS!")
        print("Game Over")
        exit()
    if check_if_board_filled(myboard):
        print_board(myboard)
        print("No winner, it's a TIE!")
        print("Game Over")
        exit()
    else:
        play_the_game_player1(myboard, difficulty)

start_the_game(game_difficulty_choice())
